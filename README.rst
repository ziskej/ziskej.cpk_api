==============
ziskej.cpk_api
==============

Volání CPK API a zpracování výsledků, základní parsování.  Vše pro potřeby
Získej.  CPK API dokumentace není součástí modulu.  CPK API v některých
připadech vrací json obsahující XML MARC21, část parsování proběhne v tomto
modulu.
