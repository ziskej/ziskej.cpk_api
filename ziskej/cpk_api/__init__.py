# -*- coding: utf-8 -*-
"""Volání CPK API a zpracování výsledků, základní parsování.  Vše pro potřeby
Získej.  CPK API dokumentace není součástí modulu.  CPK API v některých
připadech vrací json obsahující XML MARC21, část parsování proběhne v tomto
modulu.
"""

from .cpk_api import (
    CPK,
    Document,
    parse_library,
    CPKException,
    cpk_docs_overview,
    cpk_doc,
    )
