# -*- coding: utf-8 -*-
"""Volání CPK API a zpracování výsledků, základní parsování.  Vše pro potřeby
Získej.  CPK API dokumentace není součástí modulu.  CPK API v některých
připadech vrací json obsahující XML MARC21, část parsování proběhne v tomto
modulu.
"""

import logging
import os
from time import sleep
try:
    # Python 3
    from urllib.parse import urlencode
except ImportError:
    # Python 2
    from urllib import urlencode
import io
import requests
import xml.etree.ElementTree as ET
from multicache import FileCache
from pymarc import parse_xml, MARCReader, XmlHandler, parse_xml_to_array
import json


logger = logging.Logger(__name__)


# Nastavení

# CPK API, používá se produkční CPK pro všechny instance
CPK_API_URL_BASE = 'https://www.knihovny.cz/api/v1/'

# Cache
IGNORE_CACHE = True
CACHE_PATH = '/data/ziskej-async/cpk-cache'

# Celkem je něco přes 6500 knihoven v CPK, max počet pro 1 page je 50
PAGE_LIMIT = 300

# MARC21

XML_NAMESPACE = {
    'marc': 'http://www.loc.gov/MARC21/slim'
}

mappings = {
    '001': 'control_number',
    '003': 'control_number_id',
    '005': 'latest_transaction_datetime',
    '006': 'characteristics',
    '007': 'physical_description',
    '008': 'general_info',
    '010': 'loc_control_number',
    '013': 'patent_control_info',
    '015': 'national_bibliography_number',
    '040': 'cataloging_source',
    '072': 'subject_category_code',
    '080': 'universal_decimal_classification_number',
    '130': 'main_entry_uniform_title',
    '245': 'title_statement',
    '246': 'varying_form_title',
    '260': 'publication_distribution',
    '300': 'physical_description',
    '310': 'current_publication_frequency',
    '362': 'dates_of_publication',
    '500': 'general_note',
    '770': 'supplement',
    '996': '996',
}

mappings_996 = {
    'b': 'barcode',
    'c': 'storage_sig',
    'h': 'free_choice_sig',
    'd': 'volume_details',
    'v': 'year_number',
    'i': 'volume_number',
    'y': 'year',
    'l': 'branch',
    'r': 'collection',
    's': 'status',
    'n': 'borrowings',
    'a': 'availability_hours',
    'q': 'hidden',
    'w': 'administrative_id',
    'u': 'z30_item_sequence',
    'j': 'admin_base_code',
    'e': 'e',  # details unknown
    't': 't',  # details unknown
}

subfield_mappings = {
    '996': mappings_996
}


def map_996(field_data):
    if isinstance(field_data, list):
        result = []
        for item in field_data:
            subfields = item['subfields']
            subfields_dict = {}
            for subfield in subfields:
                subfields_dict.update(subfield)
            result.append({mappings_996[k]: v for k, v in subfields_dict.items()})
        return result

    else:
        subfields = field_data['subfields']
        subfields_dict = {}
        for subfield in subfields:
            subfields_dict.update(subfield)

        # dict([(key, d[key]) for d in data for key in d]
        return {mappings_996[k]: v for k, v in subfields_dict.items()}


class CPKException(Exception):
    pass


def simple_field(attr_list):
    return attr_list[0][1]

def list_field(attr_list):
    return [attr[1] for attr in attr_list]

def dict_field(attr_list):
    return {attr[0]: attr[1] for attr in attr_list}

def parse_field(attr_list):
    if len(attr_list) == 1:
        return simple_field(attr_list)

    is_listfield = True
    for attr in attr_list:
        if not attr[0] == 'a':
            is_listfield = False
    if is_listfield:
        return list_field(attr_list)

    return dict_field(attr_list)

def parse_library(lib_xml):
    root = ET.fromstring(lib_xml)
    record = root[0]
    datafields = record.findall('marc:datafield', namespaces=XML_NAMESPACE)
    data = {}
    for field in datafields:
        field_data = parse_field([(child.attrib['code'], child.text) for child in field.getchildren()])
        if field.attrib['tag'] not in data:
            data[field.attrib['tag']] = field_data
        else:
            old_data = data[field.attrib['tag']]
            if type(old_data) == list:
                data[field.attrib['tag']].append(field_data)
            else:
                data[field.attrib['tag']] = [old_data, field_data]
    return data

def get_by_key(list_of_dicts, key):
    for d in list_of_dicts:
        if key in d:
            return d[key]

class Document(object):
    def __init__(self, marcxml, logger):
        self.logger = logger
        self.marcxml = marcxml
        with io.StringIO(initial_value=marcxml) as xmlfile:
            parsed = parse_xml_to_array(xmlfile)[0]
            self.marc_dict = parsed.as_dict()
        self.fields_mapped = {}
        for field in self.marc_dict['fields']:
            tag, value = list(field.items())[0]
            if tag in mappings:
                if mappings[tag] in self.fields_mapped:
                    if not isinstance(self.fields_mapped[mappings[tag]], list):
                        self.fields_mapped[mappings[tag]] = [self.fields_mapped[mappings[tag]], ]
                    self.fields_mapped[mappings[tag]].append(value)
                else:
                    self.fields_mapped[mappings[tag]] = value
            else:
                if tag in self.fields_mapped:
                    if not isinstance(self.fields_mapped[tag], list):
                        self.fields_mapped[tag] = [self.fields_mapped[tag], ]
                    self.fields_mapped[tag].append(value)
                else:
                    self.fields_mapped[tag] = value

    @property
    def mapped_996(self):
        try:
            return map_996(self.fields_mapped['996'])
        except KeyError:
            if self.logger:
                self.logger.warning('Field 996 missing: {}'.format(self.fields_mapped))
            return {}

    @property
    def sigla(self):
        default_value = ''

        # missing sigla
        if '910' not in self.fields_mapped:
            return default_value

        # make fields list
        if not isinstance(self.fields_mapped['910'], list):
            fields = [self.fields_mapped['910'], ]
        else:
            fields = self.fields_mapped['910']

        # Example:
        # https://www.knihovny.cz/api/v1/record?id=nlk.69370&field[]=fullRecord
        # <datafield tag="910" ind1=" " ind2=" ">
        # <subfield code="a">ABA008</subfield>
        # <subfield code="b">K 38101/652</subfield>
        # <subfield code="y">x</subfield>
        # </datafield>
        # <datafield tag="910" ind1=" " ind2=" ">
        # <subfield code="a">ABE015</subfield>
        # <subfield code="b">K 107/TRS 652</subfield>
        # <subfield code="y">0</subfield>
        # </datafield>

        siglas = []
        for field in fields:
            subfs = None
            sigla = ''
            try:
                subfs = field.get('subfields', [])
            except KeyError:
                continue
            except AttributeError:
                continue
            if not subfs:
                continue
            for subf in subfs:
                if 'a' in subf:
                    sigla = subf['a']
                    break
            if not sigla:
                continue
            sigla = sigla.upper().strip()
            siglas.append(sigla)

        if not len(siglas):
            return default_value

        if len(siglas) == 1:
            return siglas[0]

        if 'ABA008' in siglas:
            return 'ABA008'

        return default_value

    @property
    def unit_ids(self):
        unit_ids = []
        try:
            fields = self.fields_mapped['996']
            if not isinstance(fields, list):
                fields = [fields, ]
        except KeyError:
            return unit_ids
        for field in fields:
            if 'subfields' not in field:
                # skip missing subfields
                continue
            subfields = field['subfields']
            for subfield in subfields:
                if 't' not in subfield:
                    continue
                unit_id = subfield['t']
                unit_ids.append(unit_id)
                break
        return unit_ids

    def serialize(self):
        """Vrátí obsah ve formě vhodné pro další převod např. do json."""
        return dict(self.fields_mapped)


class CPK(object):
    """Volání CPK API a zpracování výsledků, základní parsování.  CPK API
    dokumentace není součástí modulu.  CPK API v některých připadech vrací json
    obsahující XML MARC21, část parsování proběhne v tomto modulu.

    Nepoužívá se přímo, ale z cpk_doc, cpk_docs_overview atd.
    """

    def __init__(self, logger, url_base=CPK_API_URL_BASE):
        self.logger = logger
        self.url_base = url_base
        self.cache = FileCache(path=CACHE_PATH, ttl=3600000)
        self.debug = False

    def enable_debug(self):
        self.debug = True

    def _get_with_cache(self, url, params, purge=False):
        if self.debug:
            print ('CPK._get_with_cache({url}, {params}, purge={purge})'.format(url=url, params=params, purge=purge))
        key = "{}?{}".format(url, urlencode(params))
        if not purge and not self.debug and not IGNORE_CACHE:
            cached = self.cache.get(key)
        else:
            cached = None
        if cached:
            data = cached
        else:
            if self.debug:
                print('url: ' + str(url))
                print('params: ' + str(params))
            result_obj = requests.get(url, params=params)
            if not result_obj.ok:
                try:
                    data = result_obj.json()
                except:
                    data = None
                if self.debug:
                    print('URL: {}\n Params: {}\n Status: {}'.format(url, params, result_obj.status_code))
                if self.logger:
                    self.logger.error('URL: {} Params: {} Status: {}'.format(url, params, result_obj.status_code))
                    if data:
                        try:
                            self.logger.error(str(data))
                        except:
                            pass
                if data and isinstance(data, dict) and 'statusMessage' in data:
                    message = data.get('statusMessage', '')
                else:
                    message = ''
                raise CPKException('Request failed: http status is not OK ({status}) with message {message}'.format(
                    status=result_obj.status_code,
                    message=message,
                    ))
            else:
                if self.debug:
                    if self.logger:
                        self.logger.debug('cache miss')
                    print('cache miss')

                self._last_result = result_obj.text

                try:
                    data = result_obj.json()
                except json.decoder.JSONDecodeError as e:
                    if str(e) == 'Expecting value: line 1 column 1 (char 0)':
                        raise CPKException('Request failed: there is no json in CPK response')
                    print(str(e))
                    print(len(result_obj.text))
                    print(result_obj.text[:128])
                    raise

                if self.debug:
                    print('result_obj.json():')
                    print(str(data).encode('UTF-8'))
                if not data.get('status', None) == 'OK':
                    raise CPKException('Request failed: json status is not OK ({status})'.format(status=data.get('status', '(missing)')))
                else:
                    self.cache.put(key, data)
        return data

    def _request(self, method, params, purge=False):
        if self.debug:
            print ('CPK._request({method}, {params}, purge={purge})'.format(method=method, params=params, purge=purge))
        url = '{url_base}{method}'.format(url_base=self.url_base, method=method)

        data = self._get_with_cache(url=url, params=params, purge=purge)

        self._last_data = data
        return data

    def _libraries(self, limit=50, page=1):
        method = 'search'
        params = {
            'lookfor': 'recordtype:library',
            'sort': 'relevance',
            'page': str(page),
            'limit': str(limit),
            'prettyPrint': 'true',
            'field[]': 'id',
            }
        try:
            data = self._request(method, params)
            items = data.get('records', None)
        except CPKException as e:
            # V tomto případě nechceme vracet status Error, ale tento
            # podpožadavek brát jako vracející prázdný seznam
            if self.logger:
                self.logger.debug('_request raised CPKException')
            items = None

        if items is None:
            return []
        try:
            items = [x.get('id') for x in items]
        except:
            items_ = []
            for x in items:
                try:
                    x_id = x.get('id')
                except AttributeError as e:
                    print('AttributeError')
                    print(str(e))
                    print('items')
                    print(str(items))
                    print('_last_data')
                    print(self._last_data)
                    print('_last_result')
                    print(self._last_result)
                    if self.logger:
                        self.logger.exception('failed')
                    raise
                items_.append(x_id)
        return items

    def libraries(self, limit_on_page=50, starting_page=1, page_limit=0):
        # Validace
        if limit_on_page > 50:
            raise CPKException('Maximum limit on page exceeded')
        elif limit_on_page < 0:
            raise CPKException("Limit on page can't be lower than 0")
        elif limit_on_page == 0:
            limit_on_page = 50
        elif limit_on_page is None:
            raise CPKException("Limit on page can't be None")

        if starting_page == 0:
            raise CPKException('Pages are indexed from 1, you chose page 0')
        elif starting_page < 0:
            raise CPKException("You can't get a page with negative number")
        elif starting_page > PAGE_LIMIT:
            raise CPKException("You can't get a page with index greater than maximum page limit")
        elif starting_page is None:
            raise CPKException("Page number can't be None")

        if page_limit > PAGE_LIMIT:
            raise CPKException('Maximum page limit exceeded')
        elif page_limit < 0:
            raise CPKException("Page limit can't be lower than 0")
        elif page_limit == 0:
            page_limit = PAGE_LIMIT
        elif page_limit is None:
            raise CPKException("Page limit can't be None")

        # Postupně vše, viz page limit
        items = []
        for page in range(starting_page, page_limit+1):
            items_ = self._libraries(limit=limit_on_page, page=page)
            if len(items_) == 0:
                break
            items.extend(items_)

        # Výsledek
        return dict(status='OK', data=items)

    def library(self, library_id):
        method = 'record'
        params = {
            'id': library_id,
            'field[]': 'fullRecord',
            }
        try:
            data = self._request(method, params)
        except CPKException as e:
            if self.logger:
                self.logger.debug('_request raised CPKException')
            return dict(status='Error', error_message=str(e))
        data_records = data.get('records', [])
        if len(data_records) == 0:
            return dict(status='Error', error_message='No data')
        xml_marc = data_records[0].get('fullRecord', None)
        if not xml_marc:
            return dict(status='Error', error_message='No data')
        return dict(status='OK', data=xml_marc)

    def library_parsed(self, library_id):
        result = self.library(library_id)
        if result['status'] != 'OK':
            return result
        xml_marc = result['data']
        data = parse_library(xml_marc)
        return dict(status='OK', data=data)

    def document_info(self, document_id):
        method = 'record'
        params = {
            'id': document_id,
        }
        try:
            data = self._request(method, params)
        except CPKException as e:
            if self.logger:
                self.logger.debug('_request raised CPKException')
            return dict(status='Error', error_message=str(e))
        if not isinstance(data, dict):
            if self.logger:
                self.logger.exception('Invalid CPK resonse')
            return dict(status='Error', error_message='Invalid CPK resonse')
        if data.get('status', None) != 'OK':
            return dict(status='Error', error_message='CPK returns not OK')
        data_records = data.get('records', [])
        if len(data_records) == 0:
            return dict(status='Error', error_message='No data')
        data_record = data_records[0]
        if self.debug:
            if self.logger:
                self.logger.debug(data_record)
        return dict(status='OK', data=data_record)

    def document(self, document_id):
        method = 'record'
        params = {
            'id': document_id,
            'field[]': 'fullRecord',
        }
        try:
            data = self._request(method, params)
        except CPKException as e:
            if self.logger:
                self.logger.debug('_request raised CPKException')
            return dict(status='Error', error_message=str(e))
        if not isinstance(data, dict):
            if self.logger:
                self.logger.exception('Invalid CPK resonse')
            return dict(status='Error', error_message='Invalid CPK resonse')
        if data.get('status', None) != 'OK':
            return dict(status='Error', error_message='CPK returns not OK')
        data_records = data.get('records', [])
        if len(data_records) == 0:
            return dict(status='Error', error_message='No data')
        data_record = data_records[0]
        xml_marc = data_record.get('fullRecord', None)
        if self.debug:
            if self.logger:
                self.logger.debug(data_record)
        return dict(status='OK', data=xml_marc)

    def unit(self, unit_id):
        method = 'item'
        params = {
            'id': unit_id,
            'ext': '1',
        }
        try:
            data = self._request(method, params)
        except CPKException as e:
            if self.logger:
                self.logger.debug('_request raised CPKException')
            return dict(status='Error', error_message=str(e))
        if data.get('status', None) != 'OK':
            return dict(status='Error', error_message='CPK returns not OK')
        del data['status']
        if self.debug:
            if self.logger:
                self.logger.debug(data)
        return dict(status='OK', data=data)

    def consolidate(self, user_id, entity_id):
        method = 'consolidation_info'
        params = {
            'user_id': user_id,
            'entity_id': entity_id,
        }
        try:
            data = self._request(method, params)
        except CPKException as e:
            if self.logger:
                self.logger.debug('_request raised CPKException')
            return dict(status='Error', error_message=str(e))
        if data.get('status', None) != 'OK':
            return dict(status='Error', error_message='CPK returns not OK')
        del data['status']
        if self.debug or True:
            if self.logger:
                self.logger.debug(data)
        return dict(status='OK', data=data)

    def save_library_marc(self, library_id, filename_prefix='libraries_export/'):
        xml_marc = self.library(library_id)
        data = parse_library(xml_marc)
        sigla = data.get('SGL', 'sigla0').lower()
        filename = '{filename_prefix}{library_id}-{sigla}.xml'.format(
            filename_prefix=filename_prefix,
            library_id=library_id,
            sigla=sigla,
            )
        with open(filename, 'w', encoding='utf-8') as xml_file:
            xml_file.write(xml_marc)
        return filename

    def save_library_json(self, library_id, filename_prefix='libraries_export/'):
        xml_marc = self.library(library_id)
        data = parse_library(xml_marc)
        sigla = data.get('SGL', 'sigla0').lower()
        filename = '{filename_prefix}{library_id}-{sigla}.json'.format(
            filename_prefix=filename_prefix,
            library_id=library_id,
            sigla=sigla,
            )
        library_json = json.dumps(data, ensure_ascii=False)
        with open(filename, 'w', encoding='utf-8') as json_file:
            json_file.write(library_json)
        return filename


def nkp_mzk_ex(cpk):
    NKP_EX = False
    MZK_EX = False
    doc_ids = []
    if NKP_EX:
        doc_ids.append('nkp.NKC01-001572114')
    if MZK_EX:
        doc_ids.append('mzk.MZK01-000000456')
    for doc_id in doc_ids:
        logger.debug(" ")
        logger.debug(doc_id)
        logger.debug(" ")
        result = cpk.document(doc_id)
        # logger.debug(result)
        doc = Document(result, logger)
        #logger.debug(doc.availability)
        #logger.debug(doc.fields_mapped)
        for tag, value in parse_library(result).items():
            logger.debug(tag, value)

def libraries_export(cpk, library_ids=None, save_marc=False):
    ALL = False

    if library_ids is None:
        MZK_ID = 'library.000000732'
        NTK_ID = 'library.000000013'
        OLOMOUC_ID = 'library.000001478'

        # All libraries
        if ALL:
            result = cpk.libraries()
            #logger.debug(result[20])
            library_ids = results  # use all libraries
        else:
            library_ids = [MZK_ID, NTK_ID, OLOMOUC_ID]  # example set of libraries

    logger.debug('len(library_ids): {}'.format(len(library_ids)))  # 6601

    filenames = []
    for library_id in library_ids:
        #cpk.save_library(library_id)  # save MARC
        if ALL:
            cpk.save_library_json(library_id)  # save json
        else:
            filename = cpk.save_library_json(library_id, filename_prefix='examples/')  # save json
            filenames.append(filename)
            if save_marc:
                filename = cpk.save_library_marc(library_id, filename_prefix='examples/')  # save marc
                filenames.append(filename)

    return filenames


def cpk_doc(doc_id, logger, cpk=None):
    """Vrací spojenou verzi informací o dokumentu z CPK API - neobsahuje
    informaci o dostupnosti.

    CPK API poskytuje možnost přístupu k původnímu marc záznamu (cpk.document),
    ten ale neposkytuje např. název titulu (poskytuje sice data dostatečná pro
    jeho složení, ale opakování postupu již proběhlého v CPK je špatně).  Proto
    voláme i základní informaci o titulu tak, jak ji drží CPK
    (cpk.document_info) a tyto dvě části složíme dohromady.  Ve skutečnosti
    potřebujeme v Získej jen malou část dat, ale jejich výběr provedeme až
    v Plone app.
    """

    if cpk is None:
        cpk = CPK(logger)
    result_info = cpk.document_info(doc_id)
    if result_info['status'] != 'OK':
        return result_info
    result = cpk.document(doc_id)
    if result['status'] != 'OK':
        return result
    doc = Document(result['data'], logger)
    data = dict(
        info = result_info['data'],
        marc = doc.serialize(),
        )
    return dict(status='OK', data=data)


def cpk_docs_overview(doc_ids, logger, cpk=None):
    """Vrací základní informaci o všech alternativách, stačí pro každé doc_id
    sigla a seznam unit_ids.
    
    Požadavky na CPK jsou volány striktně za sebou.
    """

    if cpk is None:
        cpk = CPK(logger)
    data = dict()
    results = []
    for doc_id in doc_ids:
        result = cpk.document(doc_id)
        results.append(dict(
            doc_id = doc_id,
            result = result,
            ))
        if result['status'] != 'OK':
            continue
        doc = Document(result['data'], logger)
        data[doc_id] = dict(
            doc_id = doc_id,
            sigla = doc.sigla,
            unit_ids = doc.unit_ids,
            )
    if not len(data.keys()):
        return dict(status='Error', error_message='There is no valid record in CPK.', data=results)
    return dict(status='OK', data=data)
